import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NG_BOOTSTRAP_ROUTES_CONFIG} from './ng-bootstrap-routes';

@NgModule({
  imports: [RouterModule.forChild(NG_BOOTSTRAP_ROUTES_CONFIG)],
  exports: [RouterModule]
})
export class NgBootstrapRoutingModule { }
