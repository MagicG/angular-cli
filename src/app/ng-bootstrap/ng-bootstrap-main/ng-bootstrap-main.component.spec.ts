import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgBootstrapMainComponent } from './ng-bootstrap-main.component';

describe('NgBootstrapMainComponent', () => {
  let component: NgBootstrapMainComponent;
  let fixture: ComponentFixture<NgBootstrapMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgBootstrapMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgBootstrapMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
