import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgBootstrapRoutingModule } from './ng-bootstrap-routing.module';
import { NgBootstrapMainComponent } from './ng-bootstrap-main/ng-bootstrap-main.component';

@NgModule({
  declarations: [NgBootstrapMainComponent],
  imports: [
    CommonModule,
    NgBootstrapRoutingModule
  ]
})
export class NgBootstrapModule { }
