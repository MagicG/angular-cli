import {Component, OnInit} from '@angular/core';
import {MenuItem} from '../menu-item/menu-item';
import {Router, Event, NavigationEnd} from '@angular/router';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.scss']
})
export class MenuListComponent implements OnInit {
  public menuList: MenuItem [];

  constructor(private _router: Router) {
    this.menuList = [];
  }

  ngOnInit() {

    this._routerListener();
  }

  private _routerListener(): void {
    this._router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this._showCorrespondingMenu();
      }
    });
  }

  private _showCorrespondingMenu(): void {
    // switch (this._router.url) {
    //   case '/custom':
    //     this.menuList=this._buildCustomMenu();
    //     break
    //   case '/ng-bootstrap':
    //     this.menuList=this._buildNg
    //     break
    //   default:
    //     break
    // }

    if (this._router.isActive('/custom', false)) {
      this.menuList = this._buildCustomMenu();
    }
    if (this._router.isActive('/ng-bootstrap', false)) {
      this.menuList = this._buildNgBootstrapMenu();
    }
  }

  private _buildCustomMenu(): MenuItem [] {
    return [
      {
        label: 'Custom First',
        route: '/custom/first'
      },
      {
        label: 'Custom Second',
        route: '/custom/second'
      }
    ];
  }

  private _buildNgBootstrapMenu(): MenuItem[] {
    return [
      {
        label: 'NGBootstrap 1',
        route: '/ng-bootstrap/first'
      }
    ];
  }
}
