import { Component, OnInit } from '@angular/core';
import {MenuItem} from '../menu-item/menu-item';

@Component({
  selector: 'app-dashboard-navigation',
  templateUrl: './dashboard-navigation.component.html',
  styleUrls: ['./dashboard-navigation.component.scss']
})
export class DashboardNavigationComponent implements OnInit {

  public navigationItems: MenuItem[];

  constructor() {
    this.navigationItems = [];
  }

  ngOnInit() {
    this.navigationItems = [
      {
        label: 'Custom',
        route: '/custom'
      },
      {
        label: 'NG Bootstrap',
        route: '/ng-bootstrap'
      }
    ];
  }

}
