import {AppRoutingModule} from '../app-routing.module';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MainComponent} from './main/main.component';
import {HeaderComponent} from './header/header.component';
import {BodyComponent} from './body/body.component';
import {MenuListComponent} from './menu-list/menu-list.component';
import {MenuItemComponent} from './menu-item/menu-item.component';
import {DashboardNotFoundComponent} from './dashboard-not-found/dashboard-not-found.component';
import {DashboardNavigationComponent} from './dashboard-navigation/dashboard-navigation.component';

@NgModule({
  declarations: [
    BodyComponent,
    HeaderComponent,
    MainComponent,
    MenuListComponent,
    MenuItemComponent,
    DashboardNotFoundComponent,
    DashboardNavigationComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
  ],
  exports: [
    BodyComponent,
    HeaderComponent,
    MainComponent
  ]
})
export class DashboardModule {
}
