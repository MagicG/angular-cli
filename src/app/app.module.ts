import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {DashboardModule} from './dashboard/dashboard.module';

import {AppComponent} from './app.component';
import {CustomModule} from './custom/custom.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    DashboardModule,
    CustomModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
