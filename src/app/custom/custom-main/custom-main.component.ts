import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-custom-main',
  templateUrl: './custom-main.component.html',
  styleUrls: ['./custom-main.component.scss']
})
export class CustomMainComponent implements OnInit {

  public title: string;

  constructor(private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this._activatedRoute.data.subscribe((data: { title: string }) => {
      if (data) {
        this.title = data.title;
      }
    });
  }

}
