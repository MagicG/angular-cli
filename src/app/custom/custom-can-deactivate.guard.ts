import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {CustomDetailsComponent} from './custom-details/custom-details.component';

@Injectable({
  providedIn: 'root'
})
export class CustomCanDeactivateGuard implements CanDeactivate<CustomDetailsComponent> {

  canDeactivate(component: CustomDetailsComponent, currentRoute:
    ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?:
                  RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> |
    boolean | UrlTree {
    // Validar por ejemplo si el formulario tiene datos
    if (this.hasDataNotSaved()) {
      alert('por favor guarde la información antes de proceder');
      return false;
    }
    return true;
  }
  public hasDataNotSaved(): boolean {
    return Math.random() >= 0.5;
  }
  
}
