import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
  UrlSegment,
  Route,
  CanLoad,
  CanActivateChild
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(private _router: Router) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean |
    UrlTree> | boolean | UrlTree {
    let isLoggedIn = this.isLoggedIn();
    console.log(isLoggedIn);
    if (!isLoggedIn) {
      this._router.navigate(['/unknow']);
      return isLoggedIn;
    }
    return isLoggedIn;
  }
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.canActivate(childRoute, state);
  }
  public isLoggedIn(): boolean {
    return Math.random() >= 0.5;
  }
  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> |
    Promise<boolean> | boolean {
    return true;
  }
}

