import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirstComponent } from './first/first.component';
import { SecondComponent } from './second/second.component';
import { CustomMainComponent } from './custom-main/custom-main.component';
import { CustomRoutingModule } from './custom-routing.module';
import { CustomDetailsComponent } from './custom-details/custom-details.component';

@NgModule({
  declarations: [FirstComponent, SecondComponent, CustomMainComponent, CustomDetailsComponent],
  imports: [
    CommonModule,
    CustomRoutingModule
  ]
})
export class CustomModule { }
