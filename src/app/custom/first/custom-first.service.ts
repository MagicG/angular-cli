import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomFirstService {

  private _products: { id: number, name: string, description: string }[];

  constructor() {
    this._products = [
      {
        id: 1,
        name: 'leche',
        description: 'producto liquido alimenticio'
      },
      {
        id: 2,
        name: 'pan',
        description: 'producto mañanero'
      },
      {
        id: 3,
        name: 'Duraznos al jugo',
        description: 'producto enlatado refrigerado'
      },
      {
        id: 4,
        name: 'Coca Cola',
        description: 'Producto procesado'
      }
    ];
  }

  public getById(id: number): { id: number, name: string, description: string } {
    let response: { id: number, name: string, description: string } = null;
    for (let product of this._products) {
      if (product.id === id) {
        response = product;
      }
    }
    return response;
  }

  get products(): { id: number, name: string, description: string }[] {
    return this._products;
  }
}

