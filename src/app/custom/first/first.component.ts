import { Component, OnInit } from '@angular/core';
import {CustomFirstService} from './custom-first.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.scss']
})
export class FirstComponent implements OnInit {

  public products: { id: number, name: string, description: string }[];

  constructor(private _customFirstService: CustomFirstService,
              private _router: Router) {
    this.products = [];
  }
  ngOnInit() {
    this.products = this._customFirstService.products;
    console.log(this.products);
  }
  public navigate(id: number): void {
    this._router.navigate(['custom/first', id]);
  }
}
