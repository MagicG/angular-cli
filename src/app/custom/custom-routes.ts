import {Routes} from '@angular/router';
import {CustomMainComponent} from './custom-main/custom-main.component';
import {FirstComponent} from './first/first.component';
import {SecondComponent} from './second/second.component';
import {CustomDetailsComponent} from './custom-details/custom-details.component';
import {CustomCanDeactivateGuard} from './custom-can-deactivate.guard';
import {CustomGuard} from './custom.guard';

export const CUSTOM_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: CustomMainComponent,
    canActivate: [CustomGuard],
    data: {title: 'Another title!'},
    children: [
      {
        path: 'first',
        component: FirstComponent,
        children:[
          {
            path: ':id',
            component: CustomDetailsComponent,
            canDeactivate: [CustomCanDeactivateGuard]
          }
        ]
      },
      {
        path: 'second',
        component: SecondComponent
      }
    ]
  }
];
